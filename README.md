# IDS721 Individual Project 4

## Requirments
Rust AWS Lambda and Step Functions
- Rust AWS Lambda function
- Step Functions workflow coordinating Lambdas
- Orchestrate data processing pipeline

## Rust Lambda Functionality
The `process_init` function will generate a json that says "Data Preparation Complete" and "Data is ready for the next step". The `process_data` function will then receive the input json message and then generate a json that says "Processing Complete" and "Data has been processed". 

## Demo Video
[Demo Video](./demo.mov).
Or you can find it in the root directory of this repo.

## Project Steps
### Create Rust Lambda Project
1. Use `cargo lambda new <PROJECT_NAME>` to create lambda project.
2. Add necessary dependencies to `Cargo.toml`.
3. Add functionalities in `main.rs`.
4. Then create the second lambda function following the same steps as above. 

### Test Locally
1. Use `cargo lambda watch` to test lambda functions locally.
2. Use `cargo lambda invoke --data-ascii '{}'` to see the resulting json object. 

### Deploy on AWS Lambda
1. Create a role and add the following dependencies to the role: `AWSLambda_FullAccess`, `AWSLambdaBasicExecutionRole`, `IAMFullAccess` so that it can deploy and use the lambda functions properly.
2. Then build the lambda function using the following command:
    ```
    cargo lambda build --release
    ```
3. Then deploy the lambda function to AWS Lambda using the following command:
    ```
    cargo lambda deploy --region <REGION> --iam-role <ROLE_ARN>
    ```
4. Then deploy the second lambda function following the same steps as above. 

### Build Corresponding Step Functions
1. Create a new State Machine in AWS Step Functions. 
2. Implement the workflow of the step function by choosing AWS Lambda Invoke and drag it to the state machine with the order of the two function. My resulting State machine is like this: 
    ```json
    {
        "Comment": "A description of my state machine",
        "StartAt": "process_init",
        "States": {
            "process_init": {
            "Type": "Task",
            "Resource": "arn:aws:states:::lambda:invoke",
            "OutputPath": "$.Payload",
            "Parameters": {
                "Payload.$": "$",
                "FunctionName": "arn:aws:lambda:us-east-1:533267197894:function:process_init:$LATEST"
            },
            "Retry": [
                {
                "ErrorEquals": [
                    "Lambda.ServiceException",
                    "Lambda.AWSLambdaException",
                    "Lambda.SdkClientException",
                    "Lambda.TooManyRequestsException"
                ],
                "IntervalSeconds": 1,
                "MaxAttempts": 3,
                "BackoffRate": 2
                }
            ],
            "Next": "process_data"
            },
            "process_data": {
            "Type": "Task",
            "Resource": "arn:aws:states:::lambda:invoke",
            "OutputPath": "$.Payload",
            "Parameters": {
                "Payload.$": "$",
                "FunctionName": "arn:aws:lambda:us-east-1:533267197894:function:process_data:$LATEST"
            },
            "Retry": [
                {
                "ErrorEquals": [
                    "Lambda.ServiceException",
                    "Lambda.AWSLambdaException",
                    "Lambda.SdkClientException",
                    "Lambda.TooManyRequestsException"
                ],
                "IntervalSeconds": 1,
                "MaxAttempts": 3,
                "BackoffRate": 2
                }
            ],
            "End": true
            }
        }
        }
    ```
3. Execute the state machine to see whether it follows the correct order of functions and give the right output. 

## Srceenshots
### AWS Lambda Functions
![image](screenshots/lambda1.png)
![image](screenshots/lambda2.png)

### Execution Results
![image](screenshots/execution_result1.png)
![image](screenshots/execution_result2.png)

### Execution Events
![image](screenshots/execution_events.png)